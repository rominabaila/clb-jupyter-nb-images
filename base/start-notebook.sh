#!/bin/bash
set -e
# to support OpenShift's arbitrary UIDs
# more info: https://docs.openshift.com/enterprise/3.1/creating_images/guidelines.html
export USER_ID=$(id -u)
export GROUP_ID=$(id -g)
export USER_DESCRIPTION="Notebook User"
envsubst < /tmp/passwd.template > /tmp/passwd
envsubst < /tmp/group.template > /tmp/group
export LD_PRELOAD=libnss_wrapper.so
export NSS_WRAPPER_PASSWD=/tmp/passwd
export NSS_WRAPPER_GROUP=/tmp/group
# Disable core dumps
ulimit -c 0
exec jupyter labhub --NotebookApp.default_url=/lab --config=/opt/app-root/etc/jupyter_notebook_config.py $@
