# user setup script
# set git config
if [ ! "$(git config --get user.name)" ]
then
    git config --global --add user.name "${JUPYTERHUB_USER}"
fi
if [ ! "$(git config --get user.email)" ]
then
    git config --global --add user.email "${JUPYTERHUB_USER_EMAIL}"
fi
